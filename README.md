# Monitoring introduction

This proof of concept is about being familiar with Prometheus and Grafana.

It begins to answer these questions:

> How do I:
> - monitor my Docker Swarm cluster?
> - keep monitoring easy and simple?

To know more on how I proceeded and what other things I learnt, go [in the issue section](https://gitlab.com/fhuitelec-poc/docker-grafana/issues?scope=all&utf8=%E2%9C%93&state=closed&label_name[]=poc).

## Introduction

### What this introduction is not?

It is not the place where:

- ~~I will talk about PromQL~~
> - I have already done that job in the Grafana dashboard
> - I will make myself a cheatsheet and/or write a proper article when I'll comfortable with Prometheus queries/data model

- ~~I will explain how to create Grafana dashboard with Prometheus as a data source~~
> - I have just copy/pasted and [existing great board](https://grafana.com/dashboards/609) and adapted it

- ~~I talk about monitoring an application~~

### Setup

To start with a proper monitoring setup, I have read a few articles and watched some conferences and I have come with the following setup:

- [`prometheus`](https://prometheus.io/docs/introduction/overview/): monitoring server

- [`node_exporter`](https://github.com/prometheus/node_exporter): metrics about the nodes and mostly hardware usage (I/O, CPU, memory, etc.)

- [`docker native metrics`](https://docs.docker.com/engine/admin/prometheus/) & [`basi/socat`](https://hub.docker.com/r/basi/socat/): few metrics about the Swarm cluster and containers

- [`cadvisor`](https://github.com/google/cadvisor): more qualitative metrics from containers

- [`grafana`](https://grafana.com/): to have nice dashboard on metrics collected by prometheus

### Why only prometheus?

First, I like to think that, with the community behind prometheus and the the fact that it's become a member of the [Cloud Native Computing Foundation](https://www.cncf.io/), it is a promising piece of software.

Second, I'm open to other solutions, this is just an introduction to monitoring. As a matter of fact, I probably will use `influxdb`/`telegraf` to monitor `fail2ban`.

### References

##### Blog posts

- [Monitoring Docker Swarm with cAdvisor, InfluxDB and Grafana](https://botleg.com/stories/monitoring-docker-swarm-with-cadvisor-influxdb-and-grafana/) by Hanzel Jesheen
- [Docker Daemon Metrics in Prometheus](https://medium.com/@basilio.vera/docker-swarm-metrics-in-prometheus-e02a6a5745a) by Basilio Vera

##### Conferences

- [Monitoring, the Prometheus Way](https://www.youtube.com/watch?v=PDxcEzu62jk) by Prometheus co-founder at DockerCon17
- [Infrastructure and application monitoring using Prometheus](https://www.youtube.com/watch?v=5GYe_-qqP30) by `Marco Pas`

## Getting started

### Environment setup

_We are going to provision a Docker Swarm cluster with `docker-machine`_

_Adapt to whatever [`docker-machine` driver](https://docs.docker.com/machine/get-started/) you use_

1. Provision the first...
```bash
docker-machine create \
	--driver=parallels \
	--parallels-disk-size=30720 \
	--parallels-memory=4096 \
	--parallels-cpu-count=4 \
    --engine-opt experimental \
    --engine-opt metrics-addr=0.0.0.0:9323 \
	grafana-poc-1
```

2. ...and second `docker-machine`:
```bash
docker-machine create \
	--driver=parallels \
	--parallels-disk-size=30720 \
	--parallels-memory=4096 \
	--parallels-cpu-count=4 \
    --engine-opt experimental \
    --engine-opt metrics-addr=0.0.0.0:9323 \
	grafana-poc-2
```

3. Get the manager `docker` runtime environment:

```bash
eval $(docker-machine env grafana-poc-1)
```

4. Initialize Docker Swarm cluster:

```bash
docker swarm init --advertise-addr (docker-machine ip grafana-poc-1)
```

5. Make the second node join the Swarm cluster:

Copy/paste the token delivered in the last command and execute it using the `docker-machine env` of **grafana-poc-2**.

### Deploy the monitoring stack

```bash
# Launch the stack
docker stack deploy -c monitoring-stack.yaml monitor
# Watch services until they are all up
watch -n .1 docker stack ps monitor
```

### Set up grafana

- Follow [this bit of article](https://botleg.com/stories/monitoring-docker-swarm-with-cadvisor-influxdb-and-grafana/#setting-up-grafana) and add a Prometheus data source with this proxy URL: `http://prometheus:9090/`.

- Import the dashboard in `./config/grafana/swarm_cluster_dashboard.json`.

**You're good to go to play with it! 🎉 **
